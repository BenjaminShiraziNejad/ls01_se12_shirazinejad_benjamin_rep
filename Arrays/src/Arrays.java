import java.util.Scanner;
public class Arrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		    Scanner sc = new Scanner (System.in);
		    
		   
		    //Deklaration eines Arrays, 5 integer Werte
		    //Name des Arrays: "intArray"
		    int [] intArray = new int [5];
		    int [] doubleArray = {10,11,12,13,14,15};
		    
		    int index, wert;
		    
		    
		    //Speichern von Werten im Array
		    //Wert 1000 an 3. Stelle des Arrays
		    //Wert 500 an Index 4
		    intArray[2] = 1000;
		    intArray[4] = 500;
		    
		    
		    //Deklaration + Initialisierung eines Arrays in einem Schritt
		    //Name des Arrays: "doubleArray"
		    // Lšnge 3, mit 3 double Werten 
		    System.out.println("Index 0: " + intArray[0]);  
		    System.out.println("Index 1: " + intArray[1]); 
		    System.out.println("Index 2: " + intArray[2]); 
		    System.out.println("Index 3: " + intArray[3]); 
		    System.out.println("Index 4: " + intArray[4]);
		     
		    
		   
		    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
		    for (int i = 0; i < intArray.length; i++) {
		        System.out.println("Index " + i + ": "+ intArray[i]);
		        } 
		    
		    
		    //Nutzer soll einen Index angeben 
		    //und an diesen Index einen neuen Wert speichern
		    System.out.println("An welchen Index soll der neue Wert?");
		    index = sc.nextInt();
		    
		    System.out.println("Geben Sie den neuen Wert fŁr index " + index + " an:");
		    wert = sc.nextInt();
		    
		    intArray[index] = wert;
		    
		     
		     
		    
		    //Alle Werte vom Array intArray sollen ausgegeben werden
		    //Geben Sie zunšchst an, welche Werte Sie in der Ausgabe erwarten: 
		    // __   __  __  __  __  
		    
		    for (int i = 0; i < intArray.length; i++) {
		        System.out.println("Index  " + i + ": "+ intArray[i]);
		        }
		       
		    
		    //Der intArray soll mit neuen Werten gefŁllt werden
		          //1. alle Felder sollen den Wert 0 erhalten
		    
		    for (int i = 0; i < intArray.length; i++) {
		        intArray[i] = 0; 
		    }
		    
		    
		    //Der intArray soll mit neuen Werten gefŁllt werden
		          //2. alle Felder sollen vom Nutzer einen Wert bekommen
		          //nutzen Sie dieses Mal die Funktion Ąlengthď
		    for (int i = 0; i < intArray.length; i++) {
		        System.out.println("Index  " + i + ": "+ intArray[i]); 
		    }
		    
		    //Der intArray soll mit neuen Werten gefŁllt werden
		           //3. Felder automatisch mit folgenden Zahlen fŁllen: 10,20,30,40,50
		    
		        int x = 10;
		        for (int i = 0; i < intArray.length; i++) {
		           intArray[i] = x;
		           x = x+10; 
		        }
		    
		    
		    
		    
		  } // end of main
		  
		} // end of class arrayEinfuehrung

	

